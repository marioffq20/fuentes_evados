<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MI VETERINARIA</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
             
        <script type="text/javascript">
        function sonidoTortuga(){
            const sonido = document.getElementById('tortuga');
            sonido.play();
            alert("Tortuga sonando");
        }
        function sonidoMorsa(){
            const sonido = document.getElementById('morsa');
            sonido.play();
            alert("Morsa sonando");
        }
        function sonidoOveja(){
            const sonido = document.getElementById('oveja');
            sonido.play();
            alert("Oveja sonando");
        }
        function sonidoPerro(){
            const sonido = document.getElementById('perro');
            sonido.play();
            alert("Perro sonando");
        }
        function sonidoAguila(){
            const sonido = document.getElementById('aguila');
            sonido.play();
            alert("Aguila sonando");
            }
            </script>
    </head>
    <body background="resources/fondo1.jpg" style="background-size: cover">
         
        <div class="container">
            
            <div class="jumbotron">
                <h1 class="display-4"style="text-align: center">Mi Veterinaria</h1>
                <p class="lead" style="text-align: center">Mario Fuentes Queirolo</p>
                <hr class"my-4">
                
                
                <br>    
              
                    
            <div class="row">
             <div class="col-md-4 text-center">
       
             <div class="card" style="max-width: 350px;">
                 <img class="card-img-top" src="resources/tortuga.jpg">
                 <div class=card-body">
                     <p class"card-text">Tortuga</p>
                     
                 </div>
                 
                 
             </div>
                 <audio id="tortuga" src="resources/audios/Tortuga.mp3"></audio>
                 <a style="width: 30%"class="btn btn-primary btn-lg" onclick="sonidoTortuga()" href="#" role="button">Sonido</a>
                </div>
              <div class="col-md-4 text-center">
              <div class="card" style="max-width: 350px;">
                <img class="card-img-top" src="resources/morsa.jpg">
                  <div class=card-body">
                  <p class"card-text">Morsa</p>
                     
                 </div>
           
                </div>
                  <audio id="morsa" src="resources/audios/Morsa.mp3"></audio>
                  <a style="width: 30%"class="btn btn-primary btn-lg" onclick="sonidoMorsa()" href="#" role="button">Sonido</a>
            
        </div>
             <div class="col-md-4 text-center">
              <div class="card" style="max-width: 350px;">
                 <img class="card-img-top" src="resources/oveja.jpg">
                 <div class=card-body">
                 <p class"card-text">Oveja</p>
                     
                 </div>
                     
                </div>
                 <audio id="oveja" src="resources/audios/Oveja.mp3"></audio>
                 <a style="width: 30%"class="btn btn-primary btn-lg" onclick="sonidoOveja()" href="#" role="button">Sonido</a>
        </div>
            </div>
             <div class="row">
           
             <div class="col-md-6 text-center">
                

       
             <div class="card" style="max-width: 300px;">
                 <img class="card-img-top" src="resources/aguila.jpg">
                 <div class=card-body">
                     <p class"card-text">Aguila</p>
                     
                 </div>
                 
                 
             </div>
                 <audio id="aguila" src="resources/audios/Aguila.mp3"></audio>
                 
                 <a style="width: 20%"class="btn btn-primary btn-lg" onclick="sonidoAguila()" href="#" role="button">Sonido</a>
                </div>

 
              <div class="col-md-6 text-center">

         
              <div class="card" style="max-width: 300px;">
                <img class="card-img-top" src="resources/perro.jpg">
                  <div class=card-body">
                  <p class"card-text">Perro</p>
                     
                 </div>
           
                </div>
                  <audio id="perro" src="resources/audios/Perro.mp3"></audio>
                   <a style="width: 20%"class="btn btn-primary btn-lg" onclick="sonidoPerro()" href="#" role="button">Sonido</a>
                  </div>
                 </div>
               </div>
            </div>

    </body>
</html>
